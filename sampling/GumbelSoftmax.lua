--------------------------------------------------------------------------------
-- Sampling from Gumbel-Softmax distribution
--------------------------------------------------------------------------------
-- Jhosimar Arias
--------------------------------------------------------------------------------
-- Based on paper: Categorical Reparameterization with Gumbel-Softmax
--------------------------------------------------------------------------------

require "nn"
require "nngraph"

do
	
	local GumbelSoftmax = torch.class("GumbelSoftmax")
	local eps = 1e-9

	function GumbelSoftmax:__init(initial_temperature)
		self.tau = initial_temperature or 1
	end

	function GumbelSoftmax:setSeed(seed)
		torch.manualSeed(seed)
	end


	-- Gumbel Sampling: The distribution Gumbel(0, 1) can be sampled by drawing u ∼Uniform(0, 1),
	-- and computing g = − log(− log(u)).
	--
	-- Input:
	--		- n, m: number of rows and columns
	--		- minVal, maxVal (optional): used to generate uniform numbers in a range.
	--
	-- Output:
	--		- samples from Gumbel(0, 1)
	--
	function GumbelSoftmax:SampleGumbel(n, m, minVal, maxVal) -- Fix me: only 2D
		local random_uniform
		if minVal == nil and maxVal == nil then
			random_uniform = torch.rand(n, m)
		else
			random_uniform = minVal + ((maxVal - minVal) * torch.rand(n, m))
		end
		return -torch.log( -torch.log(random_uniform + eps) + eps )
	end
	

	-- Sampling from Gumbel-Softmax distribution used to approximate a categorical distribution
	--
	-- Input:
	--		- logits: logarithm of probabilities P(X=k) (tensor)
	--		- temperature: temperature used to control the approximation (constant)
	--
	-- Output:
	--		- probabilities: samples from Gumbel-Softmax distribution (tensor)
	--
	function GumbelSoftmax:GumbelSoftmaxSample(logits, temperature)
		local G = self:SampleGumbel(logits:size(1), logits:size(2))
		local y = logits + G
		return nn.SoftMax():forward( y/temperature ) 
	end


	-- Network for sampling from Gumbel-Softmax distribution used to approximate a categorical 
	-- distribution with a dynamic temperature
	--
	-- N = number of examples, K = number of classes/clusters
	--
	-- Input:
	--		- logits: logarithm of probabilities P(X=k) [NxK]
	--
	-- Output:
	--		- probability matrix: samples from Gumbel-Softmax distribution [NxK]
	--
	require '../layers/Uniform'
	function GumbelSoftmax:GumbelSoftmaxNetwork(K)
		local logits = -nn.Identity()
		 -- Create noise ε sample module from U(0, 1)
		local gumbel_sample = logits - nn.Uniform(0,1) - nn.AddConstant(eps, true)
							- nn.Log() - nn.MulConstant(-1, true)
							- nn.AddConstant(eps, true) - nn.Log()
							- nn.MulConstant(-1, true)
		self.temperature = nn.MulConstant(1 / self.tau, true) -- Temperature τ for softmax
		local y = {logits, gumbel_sample} - nn.CAddTable() - self.temperature		
		local output = y - nn.SoftMax() - nn.View(-1, K)
		return nn.gModule({logits}, {output})
	end	


	-- Network for sampling from Gumbel-Softmax distribution used to approximate a categorical 
	-- distribution with a dynamic temperature
	--
	-- N = number of examples, K = number of classes/clusters
	--
	-- Input:
	--		- logits: logarithm of probabilities P(X=k) [NxK]
	--		- gumbel_sample: samples from gumbel distribution [NxK]
	--		- temperature: temperature used to control the approximation [NxK]
	--
	-- Output:
	--		- probability matrix: samples from Gumbel-Softmax distribution [NxK]
	--
	function GumbelSoftmax:GumbelSoftmaxNetworkFull()
		local logits = -nn.Identity()
		local gumbel_sample = -nn.Identity()
		local temperature = -nn.Identity()
		local addition = {logits, gumbel_sample} - nn.CAddTable() 
		local y = {addition, temperature} - nn.CDivTable()
		local output = y - nn.SoftMax()
		return nn.gModule({logits, gumbel_sample, temperature}, {output})
	end	
end
