---------------------------------------------------------------------------------------
-- Util functions used to group data
---------------------------------------------------------------------------------------
-- Jhosimar Arias
---------------------------------------------------------------------------------------

-- Group all indices of data by label
-- 
-- Input:
--		- data_labels: list with labels [N]
--		- num_clusters (optional): number of clusters to consider in the grouping
--
-- Output: 
--		- table with {key=label and value=list of indices}
--
-- N = number of elements in the list
--
-- Example: 
--		Input  = [1,3,5,1,2,3,5]
--		Output = {1:{1,4}, 2:{5}, 3:{2,6}, 5:{3,7}}
--
--
function groupByLabel(data_label, num_clusters)
    local labels = {}
    local num_elements = data_label:size(1)
    for i = 1,num_elements do
        local key = data_label[i]
        if labels[key] == nil then
            labels[key] = {}
        end
        local key_list = labels[key]
        labels[key][#key_list + 1] = i
    end

	-- Fill with empty clusters according to num_clusteres entered
	if( num_clusters ~= nil ) then
		for i=1,num_clusters do
			if( labels[i] == nil ) then
				labels[i] = {}
			end
		end
	end

    return labels
end


-- Group true labels and indices per cluster
--
-- Given a matrix of probabilities (commonly from a softmax), get the assigned cluster by
-- taking the maximum probability, then for each assigned cluster group by labels and indices 
--
-- Input:
--		- samples_probabilities: tensor with probabilities of elements to each cluster [NxK]
--		- true_labels: labels of each element [N]
--
-- Output: 
--		- clusters: grouping of true labels by cluster [K x M_i]
--		- indices: grouping of indices by cluster	   [K x M_i]
--
-- Example of output: 
--
--		cluster[1] -> 1, 4 ,5 ,6, 5, 6 , 1		-- true labels assigned to cluster 1
--  	cluster[2] -> 5 ,1 ,6, 6, 7, 7, 7, 7	-- true labels assigned to cluster 2
--  	indices[1] -> 10, 50, 1, 15, 200, 102	-- indices assigned to cluster 1
--		indices[2] -> 2, 30, 7, 45, 100, 99		-- indices assigned to cluster 2
--
-- N = number of elements, K = number of clusters, M_i = number of elements grouped for the i-th cluster
--
function groupClusters(samples, true_labels) 
	local num_clusters = samples:size(2)
	local num_samples = samples:size(1)
	local __, labels = samples:max(2)
	
	local indices = {}
	local clusters = {} 
	
	for i=1,num_clusters do
		indices[i] = {}
		clusters[i] = {}
	end	 

	for i=1, num_samples do
		local max_index = labels[i][1]
		local label_list = clusters[max_index]   
		local size = #label_list      
		clusters[max_index][size + 1] = true_labels[i]
		indices[max_index][size + 1] = i
	end
	return clusters, indices
end


-- Group indices per cluster
--
-- Given a matrix of probabilities (commonly from a softmax), get the assigned cluster by
-- taking the maximum probability, then for each assigned cluster group by indices 
--
-- Input:
--		- samples_probabilities: tensor with probabilities of elements to each cluster [NxK]
--
-- Output: 
--		- indices: grouping of indices by cluster	   [K x M_i]
--
-- Example of output: 
--
--  	indices[1] -> 10, 50, 1, 15, 200, 102   -- indices assigned to cluster 1
--		indices[2] -> 2, 30, 7, 45, 100, 99     -- indices assigned to cluster 2
--
-- N = number of elements, K = number of clusters, M_i = number of elements grouped for the i-th cluster
--
function groupClustersByIndices(samples) 
	local num_clusters = samples:size(2)
	local num_samples = samples:size(1)
	local __, labels = samples:max(2)
	
	local indices = {}
	for i=1,num_clusters do
		indices[i] = {}
	end	 

	for i=1, num_samples do
		local max_index = labels[i][1]
		local indices_list = indices[max_index]   
		local size = #indices_list      
		indices[max_index][size + 1] = i
	end
	return indices
end


-- Group values of array
--
-- Input: Array of values [N]
-- Output: Group of values
--
-- Example of output: 
--
--		Input = [2,4,6,2,2,4,1]
--		Output = [2->3, 1->1, 4->2, 6->1] 
--
-- N = number of elements in the list, K = number of clusters
--
function groupArrayByValue(array)
	local counter = {}
	for i = 1, #array do
		if( counter[array[i]] == nil ) then counter[array[i]] = 0 end
   	    counter[ array[i] ] = counter[ array[i] ] + 1
  	end
	return counter
end


-- Group reassignment data by label
--
-- Given a table after reassignment of data {index1:[(label, value), (label2,value)], index2:(label, value), ...}
-- group the data by label and consider the assignment based on the closest element (first position of reassignment)
--
-- Input:
--		- reassignment table: for each index it contains the label it was assigned to		[NxRx2]
--							  and the distance in case of unlabeled data or -1 for labeled data
--
-- Output: 
--		- groups: indices grouped by assigned label  [K x I_i]
--		- assignment: assigned label to index only [N]
--				      difference with reassignment is that it only considers the closest element
--				  	  and only the label (index:label)
--
-- Example of output: 
--
--  	groups = { label1: [index1, index2, .., indexk], label2: [index6, index7, ..] }
--		assignment = { index1:label1, index6:label2, index2:label1, .. }
--
-- N = number of elements, K = number of clusters, I_i = number of indices grouped for the i-th cluster
--
function groupReassignmentByLabel(reassignment)
	local groups = {}
	local assignment = {}
	for index, assignment_list in pairs(reassignment) do
		local label = assignment_list[1][1] 	-- get label considering only the closest element
		if( groups[label] == nil ) then
			groups[label] = {}
		end
		table.insert(groups[label], index)
		assignment[index] = label
	end		
	return groups, assignment
end

