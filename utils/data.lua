---------------------------------------------------------------------------------------
-- Util functions used in the data
---------------------------------------------------------------------------------------
-- Jhosimar Arias
---------------------------------------------------------------------------------------

-- Create a tensor with zero values, useful for zero gradient
-- 
-- Input:
--		- tensor_size: size of the tensor
--		- use_cuda: whether to create a cuda tensor or not
--
-- Output: tensor with tensor_size and filled with zero values
--
function get_zero_gradient(size, use_cuda)
	local zeroGrad
	if( use_cuda == 1 ) then
		zeroGrad = torch.CudaTensor(size):zero()
	else
		zeroGrad = torch.DoubleTensor(size):zero()
	end
	return zeroGrad
end

-- Resize data according image dimensions required for training CNNs
-- 
-- Input:
--		- data: image data [N x H x W]
--		- input_dimension: dimension to resize
--				1 for [N x (H*W)], 2 for [N x 1 x H x W ] (grayscale image), 3 for [N x C x H x W]
--
-- Output:
--		- resized_data: obtained according to input_dimension
--		- size: depending on input_dimension
--				1 = (H * W), 2 = [H, W], 3 = [H, W]
--
function resizeData(data, inputDimension)
	if( data == nil or data:nDimension() == 0 ) then return data end
	local size
	if inputDimension == 1 then
		if data:dim() > 2 then
			data = data:resize(data:size(1), data:size(2)*data:size(3))
		end
		size = data:size(2)
	elseif inputDimension == 2 then
		size = {data:size(2), data:size(3)}
		data = data:resize(data:size(1), 1, data:size(2), data:size(3))
	elseif inputDimension == 3 then
		size = {data:size(3), data:size(4)}
	end
	return data, size
end
