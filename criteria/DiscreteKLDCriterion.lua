--------------------------------------------------------------------------------
-- KL-divergence criterion between a probability distribution and uniform prior  
--------------------------------------------------------------------------------
-- Jhosimar Arias
--------------------------------------------------------------------------------

local DiscreteKLDCriterion, parent = torch.class("nn.DiscreteKLDCriterion", "nn.Criterion")

-- Loss obtained in the forward pass
-- 
-- Input:  matrix with values of prob. distribution [NxK]
-- Output: loss value of the divergence between input and uniform prob.
--
-- N = number of examples, K = number of classes/clusters
--
function DiscreteKLDCriterion:updateOutput(input)
	-- KL = sum q(x) * log(q(x)/p(x))
	-- p(x) = 1/k
	local qx = input:clone()
	local N = qx:size(1)
	local k = qx:size(2)
	local loss = qx:cmul(torch.log(qx * k + 1e-12))     		-- KL-divergence with a prior = 1/K
	self.output = loss:sum()
	return self.output
end

-- Gradients of the divergence obtained in the backward pass
--
-- Input:  matrix with values of prob. distribution [NxK]
-- Output: matrix with gradients [NxK]
--
function DiscreteKLDCriterion:updateGradInput(input)
	local qx = input:clone()
	local N = qx:size(1)
	local k = qx:size(2)
	self.gradInput = torch.log(qx * k + 1e-12):add(1)			-- gradient of KL w.r.t input
	return self.gradInput
end

