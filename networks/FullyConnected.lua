---------------------------------------------------------------------------------------
-- Inference and Generative models based on MLPs
---------------------------------------------------------------------------------------
-- Jhosimar Arias
---------------------------------------------------------------------------------------

local Model = {
	hidden_size = 100
}


-- Recognizer/Inference Network
-- 
-- Input Params:
--		- image_size: size of the image [HxW]
--		- categorical_latent_size: categorical latent space dimension (constant = K)
--		- gaussian_latent_size: gaussian latent space dimension (constant = dz)
--
-- Input Network: Image [N x 1 x H x W] 
-- Output Network: 
--		- logits: logarithm of probabilities used to sample from gumbel [N x K]
--		- gaussian_parameters: mean and logarithm of variance
--		- features: deterministic feature representations [N x F]
--
-- H = image height, N = number of samples, K = number of categories, F = number of features
--
function Model:CreateRecognizer(input_size, num_categories, gaussian_latent_size)
	local input = - nn.Identity()
	local hidden_size = self.hidden_size

	-- Encoding from image to deterministic features
    local hidden_layer = input
                - nn.Linear(input_size, 512) - nn.BatchNormalization(512) - nn.ReLU(true)
				- nn.Linear(512, 256) - nn.BatchNormalization(256) - nn.ReLU(true)
                - nn.Linear(256, hidden_size) - nn.BatchNormalization(hidden_size) - nn.ReLU(true)

	-- Considering deep logits for better representations
	local logit = hidden_layer
				- nn.Linear(hidden_size, hidden_size) - nn.BatchNormalization(hidden_size)
				- nn.ReLU(true)
				- nn.Linear(hidden_size, hidden_size/2 ) - nn.BatchNormalization(hidden_size/2)
				- nn.ReLU(true)
				- nn.Linear(hidden_size/2 , num_categories)

	local hidden_to_gauss = hidden_layer - nn.Linear(hidden_size, hidden_size) 
				- nn.BatchNormalization(hidden_size)
				- nn.ReLU(true) 

	-- Gaussian parameters
	local mean = hidden_to_gauss
				- nn.Linear(hidden_size, gaussian_latent_size)

	local logVar = hidden_to_gauss
				- nn.Linear(hidden_size, gaussian_latent_size)	

	local meanLogVar = {mean, logVar} - nn.Identity()

	return nn.gModule({input}, {logit, meanLogVar, hidden_layer})
end


-- Generator Network
-- 
-- Input Params:
--		- output_size: size of the image [HxW]
--		- categorical_latent_size: categorical latent space dimension (constant = K)
--		- gaussian_latent_size: gaussian latent space dimension (constant = dz)
--
-- Input Network:
--		- categories: obtained from Gumbel-Softmax [N x K]
--		- features: obtained from inference network [N x F]
--		 
-- Output Network: 
--		- image generated [N x 1 x H x W] 
--
-- H = image height, N = number of samples, K = number of categories, F = number of features
--
function Model:CreateGenerator(output_size, num_categories, gaussian_latent_size)
	local hidden_size = self.hidden_size

	local categories = - nn.Identity()
	local features = - nn.Identity()

	-- Sum categories and features
	local f_to_x = features - nn.Linear(gaussian_latent_size, hidden_size)
	local c_to_x = categories - nn.Linear(num_categories, hidden_size)
	local input = {f_to_x,  c_to_x} - nn.CAddTable()

	-- Generate/Reconstruct images
    local generated = input
				- nn.Linear(hidden_size, hidden_size)  - nn.BatchNormalization(hidden_size) - nn.ReLU(true)
                - nn.Linear(hidden_size, 256) - nn.BatchNormalization(256)  - nn.ReLU(true)
				- nn.Linear(256, 512) - nn.BatchNormalization(512) - nn.ReLU(true)
                - nn.Linear(512, output_size) - nn.Sigmoid()

    return nn.gModule({categories, features} , {generated})
end

return Model
