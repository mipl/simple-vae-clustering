# Is Simple Better?: Revisiting Simple Generative Models for Unsupervised Clustering

This project is a torch implementation of our [paper](http://bayesiandeeplearning.org/2017/papers/56.pdf) presented at the Bayesian Deep Learning Workshop, NIPS 2017.

### Dependencies

1. [Torch](http://torch.ch/). Install Torch by:

       ```bash
       $ git clone https://github.com/torch/distro.git ~/torch --recursive
       $ cd ~/torch;
       $ ./install.sh      # and enter "yes" at the end to modify your bashrc
       $ source ~/.bashrc
       ```

       After installing torch, you may also need install some packages using [LuaRocks](https://luarocks.org/):

       ```bash
       $ luarocks install nn
       $ luarocks install image
       ```

       To run the code on GPU you need to install cunn and cutorch:

       ```bash
       $ luarocks install cutorch
       $ luarocks install cunn
       ```

2. [matio-ffi](https://github.com/soumith/matio-ffi.torch). It is used to load the datasets in .mat format. Run the following commands to install it:

       ```bash
       # OSX
       $ brew install homebrew/science/libmatio

       # Ubuntu
       $ sudo apt-get install libmatio2

       $ luarocks install matio
       ```

### Instructions

  * To execute a single run of the model with a specific dataset:

       ```bash
       $ ./run.sh mnist
       ```

  * To change parameter values,

       ```bash
       $ ./run.sh mnist -epoch 300 -seed 50 -verbose 1
       ```

    More details can be found in the [parameters](parameters.md) file.

To reproduce the results from our paper, you can run these models multiple times and then average the results. By default random seeds will be used, optionally you can specify different manual seeds by setting the ``-seed`` parameter.

Our tests were executed using cards *GeForce GTX 660* and *GeForce GTX 980*.

### Datasets

Tested with the following datasets: MNIST, USPS and REUTERS-10K.

* To install MNIST:

       ```bash
       $ luarocks install mnist
       ```

* The USPS and REUTERS-10K datasets can be found in the *dataset* folder.

### Disclaimer

The parameters specified in the ```run.sh``` for usps and reuters-10k datasets differ from those of the published paper due to additional experimental setup carried out in the master thesis: [Deep Generative Models for Clustering: A Semi-supervised and Unsupervised Approach](http://jhosimar.com/resources/master_thesis/jhosimar_thesis.pdf). An extended explanation of the paper can be found in this thesis.

### Citation

If you find our code useful in your researches, please consider citing:

    @inproceedings{AriasFigueroa2017,
        author = {Arias Figueroa, Jhosimar and Ram\'irez Rivera, A.},
        title = {Is Simple Better?: Revisiting Simple Generative Models for Unsupervised Clustering},
        booktitle = {Second workshop on Bayesian Deep Learning (NIPS 2017)},
        year = {2017}
    }
