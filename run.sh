#!/bin/bash

# Switch to script directory
cd `dirname -- "$0"`

if [ -z "$1" ]; then
  echo "Please enter dataset, e.g. ./run.sh dataset_name"
  exit 0
else
  DATA=$1
  shift
fi

if [ "$DATA" == "mnist" ]; then
    th main.lua -dataSet mnist -gpu 1 -K 10 -inputDimension 1 -batchSize 300 -batchSizeVal 200 -decayTemperature 1 -gpuID 1 -learningRate 0.001 -seed -1 -weightGaussian 5 -featureSize 100 -gaussianSize 150 -train 1.0 -validation 0.0 -epoch 200 "$@"
elif [ "$DATA" == "usps" ]; then
    th main.lua -dataSet usps -gpu 1 -inputDimension 1 -K 10 -batchSize 300 -batchSizeVal 200 -decayTemperature 1 -gpuID 1 -learningRate 0.001 -seed -1 -weightGaussian 5 -featureSize 150 -gaussianSize 100 -train 1.0 -validation 0.0 -epoch 200  "$@"
elif [ "$DATA" == "reuters10k" ]; then
    th main.lua -dataSet reuters10k -gpu 1 -inputDimension 1 -K 4 -batchSize 300 -batchSizeVal 200 -decayTemperature 1 -gpuID 1 -learningRate 0.001 -seed -1 -weightGaussian 5 -featureSize 150 -gaussianSize 100 -train 1.0 -validation 0.0 -epoch 200  "$@"
else
    echo "Invalid dataset. Please enter mnist, usps or reuters10k"
fi

