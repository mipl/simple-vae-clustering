--------------------------------------------------------------------------------
-- Unsupervised Categorical Variational Autoencoder - UCVAE
--------------------------------------------------------------------------------
-- Jhosimar Arias
--------------------------------------------------------------------------------

require "nn"
require "nngraph"
require "optim"
require "image"
require "./../criteria/DiscreteKLDCriterion"
require "./../sampling/GumbelSoftmax"
require "./../evaluation/ClusteringEvaluations"
require "./../criteria/KLD_Gaussian_Criterion"

do
	-- Global Variables
	local UCVAE = torch.class("UCVAEModel")
	local UCVAE_model
	local Reconstruction_criteria
	local KLD_criteria
	local options
	local Gaussian_KLD_criteria

	-- used to report losses in verbose mode
	local global_recon_loss, global_reg_loss, global_gauss_loss
	
	-- used to evaluate NMI metric
	local evaluate = require './../evaluation/NMI'

	-- UCVAE initialization
	-- Initialize criterions and neural networks based on desired options.
	--
	function UCVAE:__init(_options)

		-- Initialize criterions used in loss function and sampling
		self.sampler = GumbelSoftmax(_options.temperature)						-- Gumbel-Softmax sampling

		if _options.dataSet == "mnist" or _options.dataSet == "usps" then
			Reconstruction_criteria = nn.BCECriterion()							-- BCE reconstruction criterion
		else
			Reconstruction_criteria = nn.MSECriterion()							-- MSE reconstruction criterion
		end
		Reconstruction_criteria.sizeAverage = false
		KLD_criteria = nn.DiscreteKLDCriterion()								-- Discrete KL divergence criterion
		Gaussian_KLD_criteria = nn.KLD_Gaussian_Criterion()						-- Gaussian KL divergence criterion

		self.num_categories = _options.K
		self.gaussian_size = _options.gaussianSize
		self.use_cuda = _options.gpu
		options = _options

		-- Initialization for CUDA
		if self.use_cuda == 1 then
			require 'cutorch'
			require 'cunn'
			print('Using Cuda')
			cutorch.manualSeed(options.seed)
			Reconstruction_criteria:cuda()
			KLD_criteria:cuda()
			Gaussian_KLD_criteria:cuda()
			cutorch.setDevice(options.gpuID)
		end

		-- Load neural network used in our model
		self.network_model = require "./../networks/FullyConnected"
		self.network_model.hidden_size = options.featureSize
	end


	-- Function used in optim module for optimization
	-- 
	-- Output: 
	--		- Loss obtained after forward
	--		- Gradients obtained after backward
	-- 
	function feval(x)

		UCVAE_model:zeroGradParameters()

		-- Perform a forward operation to the model and obtain output values
		local reconstructed, soft_logits, features, meanLogVar = table.unpack( UCVAE_model:forward({batch_data, batch_gaussian_noise}) )

		local nElements = reconstructed:nElement()

		-- 1.) Reconstruction Loss = -E[ logP(x|c,f) ]
		local reconstruction_loss = Reconstruction_criteria:forward(reconstructed , batch_data) / nElements
		local reconstruction_grad = Reconstruction_criteria:backward(reconstructed , batch_data) / nElements

		-- 2.) Categorical KL-divergence = KL(Q(l|f) || P(l)) where P(l) ~ Uniform(0,1)
		local regularization_loss = options.weightCategorical * KLD_criteria:forward(soft_logits) / nElements
		local regularization_grad = options.weightCategorical * KLD_criteria:backward(soft_logits) / nElements

		-- 3.) Gaussian KL-divergence = KL(Q(z|x) || P(z)) P(z) ~ Gaussian(0,1)
		local gaussian_loss = options.weightGaussian * Gaussian_KLD_criteria:forward(meanLogVar) / nElements
		local gaussian_grad = Gaussian_KLD_criteria:backward(meanLogVar)
		gaussian_grad[1] = options.weightGaussian * gaussian_grad[1] / nElements
		gaussian_grad[2] = options.weightGaussian * gaussian_grad[2] / nElements

		-- There is no cost associated directly with the feature representations, 
		-- therefore no gradient has to be backpropagated. Features are used in plots.
		local features_grad = get_zero_gradient(features:size(), options.gpu )

		-- Backpropagate calculated gradients for each loss
		local gradLoss = {reconstruction_grad, regularization_grad, features_grad, gaussian_grad}
		UCVAE_model:backward({batch_data, batch_gaussian_noise}, gradLoss)

		-- Final loss
		local loss = reconstruction_loss + regularization_loss + gaussian_loss

		-- Keep global losses individually for printing in verbose mode
		global_recon_loss = global_recon_loss + reconstruction_loss 
		global_reg_loss = global_reg_loss + regularization_loss 
		global_gauss_loss = global_gauss_loss + gaussian_loss

		return loss, gradParams
	end


	-- Train UCVAE
	-- 
	-- Partition the data in batches and perform the training of the model according input parameters
	-- 
	-- Input: 
	--		- train_data: unlabeled data used to train the model [U x ND]
	--		- train_labels: labels of training data [U]
	--		- validation: data used to validate the model [V x ND]
	--		- validation_labels: labels of the validation data [V]
	--
	-- U = number of unlabeled elements, L = number of labeled elements, ND = dimensions of the input data
	-- V = number of validation elements
	--
	function UCVAE:train(train_data, train_labels, validation, validation_labels)
		
		-- Parameters used to train our model
		local batch_size = options.batchSize											
		local epochs = options.epoch
		local learning_rate = options.learningRate 
		local optimizer = options.optimizer
		local initial_temperature = options.temperature
		local use_pretrained = options.pretrained
		local batchSizeVal = options.batchSizeVal
		local decay_epoch = options.decayEpoch
		local lr_decay = options.lrDecay
		local use_pretrained = options.pretrained
		local proportion = options.proportion
		local decayTemperature = options.decayTemperature	
		local minTemperature = options.minTemperature
		local decayTempRate = options.decayTempRate		
		local verbose = options.verbose
		local gaussianSize = options.gaussianSize

		-- Train data size and dimension
		local input_size = train_data:size(2)
		self.num_samples = train_data:size(1)

		-- Pretrained model
		if use_pretrained == nil or use_pretrained == 0 then
			UCVAE_model = createUCVAEModel(self, input_size, self.num_categories, gaussianSize)
		end

		-- Cuda variables
		if( self.use_cuda == 1 ) then
			train_labels = train_labels:cuda()
			train_data = train_data:cuda()			
			UCVAE_model = UCVAE_model:cuda()
		end		

		-- Network parameter initialization
		params, gradParams = UCVAE_model:getParameters()		

		-- Save loss per epoch
		self.losses = {}

		-- Save metrics per epoch, used in verbose mode
		local metrics = {}

		-- Number of iterations
		for epoch = 1,epochs do

			if epoch > 1 and decay_epoch > 0 and epoch % decay_epoch == 0 then
				learning_rate = learning_rate * lr_decay -- annealing learning rate
			end

			-- Randomize data to get different batches at each epoch
			local indices_all = torch.randperm(self.num_samples):long()

			-- Case of num_samples not divisible by batch_size, repeat some random samples
			local indices_left = indices_all:size(1) % batch_size
			if( indices_left ~= 0 ) then
				local additional = batch_size - indices_left
				local additional_indices = torch.randperm(self.num_samples - indices_left ):long()
				indices_all = torch.cat( indices_all, additional_indices[{{1,additional}}])
			end

			-- Split random indices in batches
			local indices = indices_all:split(batch_size)

			-- Number of batches
			local num_batches = #indices

			-- Auxiliary variables to report losses every epoch
			local Loss = 0.0
			global_recon_loss = 0.0
			global_reg_loss = 0.0
			global_gauss_loss = 0.0

			-- True labels and gumbel probabilities for training data metrics
			local true_labels = torch.LongTensor(num_batches * batch_size)
			local predicted_probs = torch.FloatTensor(num_batches * batch_size, self.num_categories)

			--Sample noise ε ~ N(0, 1)
			local gaussian_noise = torch.randn(self.num_samples, gaussianSize)	
			if( self.use_cuda == 1 ) then
				gaussian_noise = torch.CudaTensor():resize(self.num_samples, gaussianSize):copy(gaussian_noise)
			end

			-- Create batch data tensor according size of input data
			if(train_data:nDimension() == 2 ) then
				batch_data = torch.Tensor(batch_size, input_size):typeAs(train_data)
			elseif( train_data:nDimension() == 4 ) then
				batch_data = torch.Tensor(batch_size, self.network_model.nChannels, input_size[1], input_size[2] ):typeAs(train_data)
			end

			-- Create batch noise and labels according to batch_size
			batch_labels = torch.Tensor(batch_size):typeAs(train_labels)
			UCVAE_model:training()

			-- Training in batches
 			for t,v in ipairs(indices) do
				-- Use training mode required to use Batch Normalization layer
				xlua.progress(t, #indices)

				-- Assign data to each batch
				batch_data = train_data:index(1, v)
				batch_gaussian_noise = gaussian_noise:index(1,v)
	
				-- Optimize the model with the loss function defined in feval function
				__, loss = optim[optimizer](feval, params, {learningRate = learning_rate})
				Loss = Loss + loss[1]
			end

			-- Save model according to parameter saveModelEpoch
			if( options.saveModelEpoch ~= -1 and epoch % options.saveModelEpoch == 0 ) then
				local modelName = options.fnModel .. "it" .. epoch .. ".t7"			
				print("Saving pretrained model of epoch: " .. epoch .. " in file: ".. modelName .. "...")
				self:saveModel(modelName)
			end

			self.losses[#self.losses + 1] = Loss		

			-- Training loss information			
			print(string.format("Epoch: %d  Loss: %.6f", epoch, Loss/num_batches))

			if( verbose == 1 ) then
				print(string.format("Reconstruction: %.5f   Categorical: %.5f   Gaussian: %.5f", 
						global_recon_loss/num_batches, global_reg_loss/num_batches, global_gauss_loss/num_batches) )
			end

			-- Predict probabilities per cluster for training data
			local predicted_probs, _ = predictData(UCVAE_model, train_data, batchSizeVal, 
												   self.num_categories, self.use_cuda, self.gaussian_size)
			
			-- Training metrics
			local accuracy_training = AAE_Clustering_Criteria(predicted_probs, train_labels:float())
			local nmi_training = evaluate.NMIFromProbabilities(predicted_probs, train_labels:float())
			print(string.format("Training   Accuracy: %.5f          NMI: %.5f", accuracy_training, nmi_training))         

			-- Save metrics every 10 epochs to print in verbose mode
			if( epoch == 1 or epoch % 10 == 0 ) then
				metrics[ epoch ] = {}
				metrics[ epoch ][1] = accuracy_training 
			end

			-- Validation data information and performance
			if( validation ~= nil and validation:size(1) > 0 ) then
				
				-- Predict categories and features for validation data
				local categories, features = predictData(UCVAE_model, validation, batchSizeVal, 
														 self.num_categories, self.use_cuda, self.gaussian_size)

				-- Save data each saveEpoch iteration
				if( options.saveEpoch > 0 and options.saveEpoch <= epochs and epoch % options.saveEpoch == 0 ) then
					----------------------------------------------------------------------------------------
					local validation_num_samples = validation:size(1)
					local validation_true_label = validation_labels[{{1,validation_num_samples}}]

					local fileNFeaturesName = options.saveResultFile .. "_Feat_".. epoch.. ".txt"
					local fileCategoriesName = options.saveResultFile .. "_Cat_".. epoch.. ".txt"
					
					-- Predicted label - true label
					local filePredictionsName = options.saveResultFile .. "_Pred_" .. epoch .. ".txt"
					local __, predictedLabels = categories:max(2)
					local predictionsFile = io.open(filePredictionsName , "w")

					print("Saving results of epoch: " .. epoch .. " in file: ".. options.saveResultFile .. "...")
					local featuresFile = io.open(fileNFeaturesName, 'w')
					local categoriesFile = io.open(fileCategoriesName, 'w')

					for i=1,validation_num_samples do 
						for j=1,features:size(2) do
							featuresFile:write(features[i][j] .. " ")
						end
						featuresFile:write(validation_true_label[i] .. "\n")

						for j=1,self.num_categories do
							categoriesFile:write(categories[i][j] .. " ")
						end
						categoriesFile:write(validation_true_label[i] .. "\n")

						predictionsFile:write(predictedLabels[i][1] .. " " .. validation_true_label[i] .. "\n")
					end

					predictionsFile:close()

					categoriesFile:close()
					featuresFile:close()
					-----------------------------------------------------------------------------------------
				end

				-- Validation metrics
				local accuracy = AAE_Clustering_Criteria(categories, validation_labels)
				local nmi = evaluate.NMIFromProbabilities(categories, validation_labels)
				if options.validation == 0.0 then
					print(string.format("Test       Accuracy: %.5f          NMI: %.5f", accuracy, nmi))
				else
					print(string.format("Validation Accuracy: %.5f          NMI: %.5f", accuracy, nmi))
				end
				-- Save metrics every 10 epochs to print in verbose mode
				if( epoch == 1 or epoch % 10 == 0 ) then
					metrics[ epoch ][2 ] = accuracy
					metrics[ epoch ][3 ] = nmi  
				end
			end

			-- Print log of metrics (verbose mode)
			if( verbose == 1 ) then
				for iteration, values in pairs(metrics) do
					io.write(string.format("(%d): TACC: %.5f  VACC: %.5f VNMI: %.5f, ", 
											iteration, values[1], values[2], values[3]))
				end
            	io.write("\n")
			end

			-- Annealing gumbel temperature rate -> alpha = alpha0 * exp(-kt). alpha0=initial, t=iteration/epoch
			if( decayTemperature == 1 ) then
				-- Exponential annealing for temperature
				self.sampler.tau = math.max( initial_temperature * math.exp( -decayTempRate * epoch ), minTemperature )
				self.sampler.temperature.constant_scalar = 1 / self.sampler.tau
				if( verbose == 1 ) then 
					print("Gumbel Temperature: " .. self.sampler.tau)
				end
			end
		end
	end


	-- Gaussian sampler
	-- 
	-- Reparameterization trick given gaussian parameters
	--
	-- Params: 
	--		- gaussian latent space dimension (dz)
	--
	-- Input Network: 
	--		- gaussian_parameters: mean [N x dz] and logarithm of variance [N x dz]
	--		- gaussian_noise: random noise obtained from Gaussian(0,1) [N x dz]
	--
	-- Output Network:
	--		- gaussian sample
	--
	-- N = number of samples per batch
	--
	function gaussian_sampler(latent_dim)
		local z_latent = - nn.Identity()
		local noise = - nn.Identity() 

		local mean = z_latent - nn.SelectTable(1)
		local logVar = z_latent - nn.SelectTable(2)

		local sigma = logVar 
							- nn.MulConstant(0.5) -- Compute 1/2 log σ^2 = log σ
							- nn.Exp()            -- Compute σ

		local sigma_noise = {sigma, noise} - nn.CMulTable() -- Compute σε

		-- Reparameterization trick 
		-- N(z; μ, σI) = μ + σε
		local sample = {mean, sigma_noise} - nn.CAddTable() - nn.View(-1, latent_dim)

		return nn.gModule({z_latent, noise}, {sample})
	end


	-- UCVAE Model
	-- 
	-- Partition the data in batches and perform the training of the model according input parameters
	-- 
	-- Params:
	--		- input_size: size of input data used in creation of networks
	--		- num_categories: number of categories used in creation of networks
	--		- gaussian_size: gaussian latent space dimension (dz)
	--
	-- Input Network: 
	--		- input_data: data to pass throught the model [N x ND]
	--		- gaussian_noise: random noise obtained from Gaussian(0,1) [N x dz]
	--	
	-- Output Network:
	--		- reconstructed: reconstructed image after forward [N x ND]
	--		- soft_logits: softmax of logits i.e. probabilities [N x K]
	--		- gaussian_parameters: mean [N x dz] and logarithm of variance [N x dz]
	--
	-- N = number of samples per batch, ND = dimensions of the input data
	-- K = number of clusters, F = number of features
	--
	function createUCVAEModel(self, input_size, num_categories, gaussian_size)
		local input_data = - nn.Identity()
		local gaussian_noise = - nn.Identity()

		-- Inference network receives input_data
		local recognizer = self.network_model:CreateRecognizer(input_size, num_categories, gaussian_size)
		local output_recognizer = input_data - recognizer

		-- Obtain logits, gaussian parameters and features
		local logits = output_recognizer - nn.SelectTable(1)
		local meanLogVar = output_recognizer - nn.SelectTable(2)
		local features = output_recognizer - nn.SelectTable(3)

		-- Get probabilities from logits by using SoftMax
		local soft_logits = logits - nn.View(-1, num_categories) - nn.SoftMax()

		-- Get categorical data using gumbel-softmax distribution
		local categorical = logits - self.sampler:GumbelSoftmaxNetwork(num_categories)

		-- Generator network receives categorical probabilities and features
		self.generator = self.network_model:CreateGenerator(input_size, num_categories, gaussian_size)
		local gaussian_sample = {meanLogVar, gaussian_noise} - gaussian_sampler(gaussian_size)
		local reconstructed = {categorical, gaussian_sample} - self.generator

		return nn.gModule({input_data, gaussian_noise}, {reconstructed, soft_logits, features, meanLogVar})
	end


	-- Data Reconstruction
	-- 
	-- Obtain reconstructed image for different inputs after training model
	-- 
	-- Input: 
	--		- input_data: data to pass throught the model [1 x ND] 
	--	
	-- Output:
	--		- reconstructed: reconstructed image after forward [1 x ND]
	--
	-- N = number of samples per batch, ND = dimensions of the input data
	--
	function UCVAE:reconstruct(input_data)
		-- Generate gaussian noise
		local gaussian_noise = torch.randn(1, self.gaussian_size)

		-- Cuda variables
		if self.use_cuda == 1 then
			input_data = input_data:cuda() 
			gaussian_noise = torch.CudaTensor():resize(1, self.gaussian_size):copy(gaussian_noise)
		end

		-- Used to evaluate model required to use Batch Normalization layer
		UCVAE_model:evaluate()

		-- Obtain reconstructed image
		local reconstructed, _, _ = table.unpack(UCVAE_model:forward({input_data, gaussian_noise}))
		if( inputDimension == 2 ) then -- 1 x 1 x height x width 
			return reconstructed[1]:double()
		end
		
		return reconstructed:double()
	end


	-- Predict features and categories for a given input data
	-- 
	-- Obtain both categories and features after forward pass
	-- 
	-- Input: 
	--		- input_data: data to pass throught the model [N x ND] 
	--		- batch_size: used to predict values in batches
	--	
	-- Output:
	--		- categorical: probabilities after gumbel-softmax [N x K]
	--		- features: determinisitc features [N x F]
	--
	-- N = number of samples per batch, ND = dimensions of the input data
	-- K = number of clusters, F = number of features
	-- 
	function UCVAE:getLatentSpace(input_data, batch_size)
		return predictData(UCVAE_model, input_data, batch_size, self.num_categories, self.use_cuda, self.gaussian_size)
	end

	-- Save trained model, useful for pretraining
	-- 
	-- Input: path to save the model
	--
	function UCVAE:saveModel(path) 		
		UCVAE_model:clearState() 		
		torch.save(path, UCVAE_model) 	
	end 	

	-- Load trained model
	-- 
	-- Input: path to load the trained model
	--
	function UCVAE:loadModel(path) 		
		UCVAE_model = torch.load(path) 	
	end
	

	-- Predict features and categories for a given input data
	-- 
	-- Obtain both categories and features after forward pass
	-- 
	-- Input: 
	--		- model after training
	--		- input_data: data to pass throught the model [N x ND] 
	--		- batch_size: used to predict values in batches
	--		- num_categories: number of categories/clusters
	--		- use_cuda: whether to use cuda or not (it should follow model training)
	--		- gaussian_size: dimension of gaussian latent space
	--	
	-- Output:
	--		- categorical: probabilities after gumbel-softmax [N x K]
	--		- features: determinisitc features [N x F]
	--
	-- N = number of samples per batch, ND = dimensions of the input data
	-- K = number of clusters, F = numbe of features
	-- 
	function predictData(UCVAE_model, input_data, batch_size, num_categories, use_cuda, gaussian_size)
		-- default batch size prediction
		if( batch_size == nil ) then batch_size = 100 end

		local num_samples = input_data:size(1)

		-- generate gaussian noise
		local gaussian_noise = torch.randn(num_samples, gaussian_size)
	
		-- Cuda variables
		if use_cuda == 1 then	
			input_data = input_data:cuda() 
			gaussian_noise = torch.CudaTensor():resize(num_samples, gaussian_size):copy(gaussian_noise)
		end
		
		-- Split in batches following the input order
		local indices_all = torch.linspace(1, num_samples, num_samples):long()
		local indices = indices_all:split(batch_size)

		-- Tensor to save the results
		local categorical = torch.DoubleTensor()
		local features = torch.DoubleTensor()		

		-- Used to evaluate model required to use Batch Normalization layer
		UCVAE_model:evaluate()
		
		-- Testing in batches
		for t,v in ipairs(indices) do
			local batch_data = input_data:index(1, v)
			local batch_gaussian_noise = gaussian_noise:index(1,v)
			local _, logits, feat, _ = table.unpack(UCVAE_model:forward({batch_data, batch_gaussian_noise}))

			categorical = torch.cat(categorical, logits:double(),1)
			features = torch.cat(features, feat:double(), 1)
		end
		return categorical, features
	end	


	-- Predict features for a given input data
	-- 
	-- Obtain features after forward pass
	-- 
	-- Input: 
	--		- input_data: data to pass throught the model [N x ND] 
	--	
	-- Output:
	--		- features: deterministic features [N x F]
	--
	-- N = number of samples per batch, ND = dimensions of the input data
	-- K = number of clusters, F = numbe of features
	-- 
	function UCVAE:getFeatures(input_data)

		-- Generate gaussian noise ε ~ N(0, 1)
		local gaussian_noise = torch.randn(input_data:size(1), self.gaussian_size)

		-- Cuda variables
		if self.use_cuda == 1 then	
			input_data = input_data:cuda() 
			gaussian_noise = gaussian_noise:cuda()
		end 

		-- Obtain features from input data
		local _, _, features, _ = table.unpack( UCVAE_model:forward({input_data, gaussian_noise}) )
		return features
	end


	-- Generate random data after training
	-- 
	-- Obtain generated image after forward pass
	-- 
	-- Input: 
	--		- features: data features, usually after forward [N x F]
	--		- categorical: one-hot representing category/cluster to generate [N x K]
	--	
	-- Output:
	--		- generated images [N x ND]
	--
	-- N = number of samples per batch, ND = dimensions of the training data
	-- K = number of clusters, F = numbe of features
	--
    function UCVAE:generate(features, category)
		local one_hot = getOneHotRepeat(category, features:size(1), self.num_categories)
		UCVAE_model:evaluate()
		return self.generator:forward({one_hot:cuda(), features:cuda()})
    end


	-- Generate a one-hot with repeated values
	-- 
	-- Input: 
	--		- cluster: constant indicating the category/cluster [1,M]
	--		- n, m: rows and columns of the one-hot tensor
	--	
	-- Output:
	--		- one-hot tensor with 1 at for cluster specified and 0 for the rest  [N x M]
	--
	function getOneHotRepeat(cluster, n, m)
		local indices = torch.LongTensor({cluster}):view(-1,1)
		local one_hot = torch.zeros(1, m)
 	    one_hot:scatter(2, indices, 1)
    	return one_hot:repeatTensor(n,1)
    end	


	-- Generate a one-hot according specified values
	-- 
	-- Input: 
	--		- cluster_values: list of values indicating the category/cluster [N, M]
	--		- n, m: rows and columns of the one-hot tensor
	--	
	-- Output:
	--		- one-hot tensor with 1 at for each specified cluster and 0 for the rest  [N x M]
	--
	function getOneHot(cluster_values, n, m)
		local indices = torch.LongTensor(cluster_values):view(-1,1)
		local one_hot = torch.zeros(n, m)
		one_hot:scatter(2, indices, 1)
		return one_hot
    end	

end
