----------------------------------------------------------
-- Author: Jhosimar George Arias Figueroa
----------------------------------------------------------

require "image"
require "./data_partition/FoldIterator"
require "./utils/grouping"
require "./utils/data"

-- command line options --
local cmd = torch.CmdLine()

----------------------------------------------------------
-- Input Parameters
----------------------------------------------------------
--Dataset
cmd:option('-dataSet', 'mnist', 'Dataset used')
cmd:option('-seed', -1, 'random seed')

--GPU
cmd:option('-gpu', 1, 'Using Cuda, 1 to enable')
cmd:option('-gpuID', 1, 'Set GPU id to use')

--Training
cmd:option('-batchSize', 300, 'Training batch Size')
cmd:option('-optimizer', 'adam', 'Optimizer')
cmd:option('-epoch', 200, 'Number of Epochs')
cmd:option('-decayEpoch', -1, 'Reduces the learning rate every decayEpoch, -1 for no decay')
cmd:option('-learningRate', 0.001, 'Learning rate for training')
cmd:option('-lrDecay', 0.5, 'Learning rate decay for training')

--Architecture
cmd:option('-K', 10, 'Number of clusters')
cmd:option('-inputDimension', 1, 'Dimension of the input vector into the network (e.g. 2 for height x width)')
cmd:option('-featureSize', 150, 'Size of the hidden layer that splits gaussian and categories')
cmd:option('-gaussianSize', 100, 'Size of the gaussian learnt by the network')

--Pretrained model
cmd:option('-pretrained', 0, 'Use pretrained model')
cmd:option('-saveModel',0, 'Save model in -fnModel file after finishing training')
cmd:option("-fnModel", "pretrained/model.t7", "Filename where model will be saved")
cmd:option("-saveModelEpoch", -1, "Save model at each specified epoch")

--Partition parameters
cmd:option('-batchSizeVal', 1000, 'Batch Size of validation data')
cmd:option('-batchSizeTest', 1000, 'Batch Size of test data')
cmd:option('-nFolds', 1, 'Number of folds')
cmd:option('-train', 0.8, 'Training partition (0.0-1.0)')
cmd:option('-validation', 0.2, 'Validation partition (0.0-1.0)')

--Gumbel parameters
cmd:option('-temperature', 1, 'Initial temperature used in gumbel-softmax (recommended 0.5-1.0)')
cmd:option('-decayTemperature', 1, 'Set 1 to decay gumbel temperature every epoch')
cmd:option('-minTemperature', 0.5, 'Minimum temperature of gumbel-softmax after annealing')
cmd:option('-decayTempRate' , 0.00693, 'Temperature decay rate every epoch')

--Loss function parameters
cmd:option("-weightGaussian", 5, "Weight of Gaussian regularization")
cmd:option("-weightCategorical", 1, "Weight of Categorical regularization")

--save results
cmd:option("-fnResults", "test_results", "Filename prefix where final results will be saved")
cmd:option("-saveEpoch", -1, "To save results obtained at each -saveEpoch")
cmd:option("-saveResultFile", "validation_results","Filename where results will be saved at each -saveEpoch")

--others
cmd:option('-verbose', 0 , "Print extra information every epoch.")

local opt = cmd:parse(arg)

if opt.verbose == 1 then
	print(opt)
end

-- If not seed was specified, select a random one
if opt.seed == -1 then
	opt.seed = torch.random()
	print("Using random seed: " .. opt.seed )
end

-- Set seed
torch.manualSeed(opt.seed)

-- Load model
require "./model/unsupervised"

----------------------------------------------------------
-- Read data
----------------------------------------------------------
local test_num_samples
local train_data, train_label
local test_data, test_label
local validation_data, validation_label
local n_samples, m_features

if opt.dataSet == "reuters10k" then
	print("Loading reuters10k dataset...")
	--https://github.com/soumith/matio-ffi.torch
	local matio = require 'matio'
	local data = matio.load('./dataset/reuters10k.mat')
	
	-- split original data: 80% for train and 20% for test
	train_data = data.X[{{1,8000}}]
	train_label = data.Y[1][{{1,8000}}]:add(1)

	test_data = data.X[{{8001,10000}}]
	test_label = data.Y[1][{{8001,10000}}]:add(1)

	n_samples = train_data:size(1)
	num_features = train_data:size(2)
	test_num_samples = test_data:size(1)

elseif opt.dataSet == "usps" then
	print("Loading usps dataset...")
	--https://github.com/soumith/matio-ffi.torch
	local matio = require 'matio'
	local data = matio.load('./dataset/usps.mat')
	
	-- Load train and test dataset
	train_data = data.trainData.data:double():div(2) 	
	train_label = data.trainData.labels:view(data.trainData.labels:size(2)):add(1)
	test_data = data.testData.data:double():div(2) 	
	test_label = data.testData.labels:view(data.testData.labels:size(2)):add(1)

	n_samples = train_data:size(1)
	num_features = train_data:size(2)
	test_num_samples = test_data:size(1)

else
	print("Loading mnist dataset...")
	local mnist = require "mnist"

	-- Load train and test dataset
	train_data = mnist.traindataset().data:double():div(255)                -- normalized [0, 1]
	train_label = mnist.traindataset().label:add(1)                         -- label from [0,9] -> [1, 10]
	test_data = mnist.testdataset().data:double():div(255)                  -- normalized [0, 1]
	test_label = mnist.testdataset().label:add(1)                           -- label from [0,9] -> [1, 10]
	
	test_num_samples = mnist.testdataset().size                             -- test sample size
	n_samples = mnist.traindataset().size                                   -- train sample size
	m_features = mnist.traindataset().data:size(2) * mnist.traindataset().data:size(3)
end

----------------------------------------------------------
-- Data Partition
----------------------------------------------------------
if opt.train <= 1 then

	-- Partition based on percentages
	local options = {
		seed = opt.seed,
		nFolds = opt.nFolds,
		train = opt.train,
		validation = opt.validation
	}

	-- If validation is considered, partition the data accordingly
	if( opt.validation ~= 0.0 ) then
		local foldIterator = FoldIterator(train_data, train_label, options)
		local train, validation, test = foldIterator:next()
		train_data = train.data
		train_label = train.label
		validation_data = validation.data
		validation_label = validation.label
	else
		-- If validation is not consider, use test as validation
		validation_data = test_data:clone()
		validation_label = test_label:clone()
	end

else
	-- Partition based on number of elements
	local indices_random = torch.randperm(n_samples):long()
	
	-- If validation is considered, partition the data accordingly
	if( opt.train < n_samples ) then
		validation_data = train_data:index(1, indices_random[{{opt.train + 1, n_samples}}])
		validation_label = train_label:index(1, indices_random[{{opt.train + 1, n_samples}}])
	else
		-- If validation is not consider, use test as validation
		validation_data = test_data:clone()
		validation_label = test_label:clone()
	end

	train_data = train_data:index(1,indices_random[{{1,opt.train}}])
	train_label = train_label:index(1,indices_random[{{1,opt.train}}])
end

----------------------------------------------------------
-- Preparing Data
----------------------------------------------------------
local inputDimension = opt.inputDimension
train_data = resizeData(train_data, inputDimension)
validation_data = resizeData(validation_data, inputDimension)
test_data = resizeData(test_data, inputDimension)

if opt.verbose == 1 then
	print("Train size: " .. train_data:size(1) .. "x" .. train_data:size(2))
	if opt.validation ~= 0 then
		print("Validation size: " .. validation_data:size(1) .. "x"..validation_data:size(2))
	end
	print("Test size: " .. test_data:size(1) .. "x" .. test_data:size(2))
end

----------------------------------------------------------
-- Training Model
----------------------------------------------------------
local model = UCVAEModel(opt)

-- Use pretrained
if( opt.pretrained == 1 ) then
	print("Using pretrained model")
	model:loadModel(opt.fnModel)
end

-- Train VAE
model:train(train_data, train_label, validation_data, validation_label)

-- Save model
if( opt.saveModel == 1 ) then
	model:saveModel(opt.fnModel)
end

----------------------------------------------------------
-- Testing Model
----------------------------------------------------------
print("\nTesting...")

-- Obtain probability values from categorical distribution and deterministic features
local categories, features = model:getLatentSpace(test_data, opt.batchSizeTest )

local num_categories = opt.K

if( #opt.fnResults ~= 0 ) then
	print("Saving features and categorical values...")
	-- Save features and gumbel results (probabilities w.r.t. each cluster)
	local fileNFeaturesName = opt.fnResults .. "_Feat".. ".txt"
	local fileCategoriesName = opt.fnResults .. "_Cat".. ".txt"			
	local featuresFile = io.open(fileNFeaturesName, 'w')
	local categoriesFile = io.open(fileCategoriesName, 'w')

	for i=1,test_num_samples do 
		for j=1,num_categories do
			categoriesFile:write(categories[i][j] .. " ")
		end
		categoriesFile:write(test_label[i] .. "\n")	
	
		for j=1,features:size(2) do
			featuresFile:write(features[i][j] .. " ")
		end
		featuresFile:write(test_label[i] .. "\n")
	end
	categoriesFile:close()
	featuresFile:close()
end

-------------------------------------------------------------
-- Confusion Table
-------------------------------------------------------------
local test_clusters = groupClusters(categories, test_label)

io.write("            ")
for i = 1,num_categories do
   io.write(string.format("  %d    ", i - 1 ))
end
io.write("\n")

for i = 1, num_categories do
   io.write(string.format("Cluster-%d   ", i))
   if( test_clusters[i] == nil ) then
   	  print("No elements in cluster " .. i )
   else
	   local counter = {}
	   for j = 1, num_categories do
		  counter[j] = 0
	   end

	   for j = 1, #test_clusters[i] do
		  if( counter[test_clusters[i][j]] == nil ) then counter[test_clusters[i][j]] = 0 end
		   counter[ test_clusters[i][j] ] = counter[ test_clusters[i][j] ] + 1
	   end
	
	   for j = 1, num_categories do
		   if #test_clusters[i] > 0 then
		      io.write( string.format("%.2f   ", (counter[j]/#test_clusters[i]) ))
	       else
              io.write( string.format("%.2f   ", 0.0))
           end
       end
	   io.write("\n")
   end
end


-------------------------------------------------------------
-- Metrics
-------------------------------------------------------------
-- Calculate Clustering Accuracy (ACC)
local accuracy = AAE_Clustering_Criteria(categories, test_label)

-- Calculate Normalized Mutual Information (NMI)
local evaluate = require './evaluation/NMI'
local nmi = evaluate.NMIFromProbabilities(categories, test_label)

print(string.format("\nTest       Accuracy: %.5f          NMI: %.5f\n", accuracy, nmi))
