<a name="Parameters"></a>
# Parameters

Our model is based on different parameters that can be changed and executed by the `main.lua` file.

* GPU parameters:

  * `gpu`: Enables cuda, 1 for enabling and 0 for disabling. (`default: 1`);
  * `gpuID`: If one has multiple-GPUs, you can switch the default GPU. The GPU IDs are 1-indexed, so using GPU 3 can be specified as `-gpuID 3`. (`default: 1`).


* Training parameters:
  * `batchSize`: Batch size used in training. (`default: 300`);
  * `optimizer`: Specifies the method used in the optimization, you can specify it by names, so using adam can be specified as `-optimizer adam`. Different methods can be found in [optim](https://github.com/torch/optim) library. (`default: adam`);
  * `epoch`: Number of epochs to consider during training. (`default: 200`);
  * `decayEpoch`: Reduces the learning rate every `decayEpoch`. Used in *step decay* of the learning rate. For instance, `-decayEpoch 10` reduces the learning rate by `-lrDecay` every 10 epochs. (`default: -1`);
  * `learningRate`: Initial learning rate for training. (`default: 0.001`);
  * `lrDecay`: Decreases the learning rate by this value, it is used jointly with `decayEpoch`. (`default: 0.5`).


* Architecture parameters:

  * `K`: Number of clusters. (`default: 10`);
  * `inputDimension`: Dimension of the input vector into the network (`default: 1`). It converts the data size to one of the possible sizes:
        * 1 resizes to \[N x (H*W)\] (useful in MLP networks).
        * 2 resizes to \[N x 1 x H x W\] (useful in CNN networks - grayscale images).
        * 3 resizes to \[N x C x H x W\] (useful in CNN networks - color images).
        - N: number of samples, H: height, W: width and C: number of channels;
  * `featureSize`: Size of the deterministic feature vector learnt by the network. (`default: 150`);
  * `gaussianSize`: Size of the gaussian latent vector learnt by the network. (`default: 100`);


* Partition parameters:

  * `batchSizeVal`: Batch size used in validation data. (`default: 1000`);
  * `batchSizeTest`: Batch size used in test data. (`default: 1000`);
  * `train`: Percentage of data to consider for training. Range of values [0.0 - 1.0]. (`default: 0.8`);
  * `validation`: Percentage of data to consider for validation. Range of values [0.0 - 1.0]. (`default: 0.2`).

  For a partition with 80% for training and 20% for validation we specify `-train 0.8` and `-validation 0.2`. *Both train and validation must be set*.

* Gumbel parameters:

  * `temperature`: Initial temperature used in gumbel-softmax. Recommended range of values [0.5-1.0]. (`default: 1`);
  * `decayTemperature`: Set 1 to decay gumbel temperature every epoch. (`default: 1`);
  * `minTemperature`: Minimum temperature value after annealing. (`default: 0.5`);
  * `decayTempRate`: Rate of temperature decay every epoch. (`default: 0.00693`).

  Temperature decay is based on *Exponential decay* -> ``newTemperature = temperature * exp(-decayTempRate * currentEpoch)``. The value of `decayTempRate` can be obtained from that formulation according to the number of epochs used in training. By default the temperature decreases from 1 to 0.5 in the first 100 epochs.

* Loss function paremeters:

  * `weightGaussian`: Weight of the gaussian regularization loss. (`default: 5`);
  * `weightCategorical`: Weight of the categorical regularization loss. (`default: 1`).


* Result parameters:

  * `fnResults`: Filename perfix to save categories and features values after testing. (`default: test_results`);
  * `saveEpoch`: Save categories and features every `saveEpoch`. Range of values [1,number of epochs]. Set -1 to not consider this parameter. (`default: -1`);
  * `saveResultFile`: Filename where categories and features will be saved every `-saveEpoch`. (`default: validation_results`).


* Pretrained parameters:

  * `pretrained`: Set 1 to use a pretrained model. (`default: 0`);
  * `saveModel`: Set 1 to save the current model every `-saveModelEpoch`. (`default: 0`);
  * `fnModel`: Filename where the model will be loaded/saved, it must have .t7 extension. (`default:pretrained/model.t7`);
  * `saveModelEpoch`: Save model every `saveModelEpoch`. Range of values [1, number of epochs]. Set -1 to not save the model. (`default: -1`).


* Other parameters:

  * `dataSet`: Dataset to use. It can be mnist, usps or reuters10k. (`default: mnist`);
  * `seed`: Seed used to initialize random weights and partitions. Set -1 for random seed. (`default: -1`);
  * `verbose`: Print additional details while training. (`default: 0`).
